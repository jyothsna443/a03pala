# jyothsna Guestbook Example
A simple guest book using Node, Express, BootStrap, EJS
A guestbook page is created and added to the previously created project
The guestbook page is also added alongwith the home,choice and contact page.
Open a command window in your c:\44563\ folder.

Run npm install to install all the dependencies in the package.json file.

Run node gbapp.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node gbapp.js
```

Point your browser to `http://localhost:8081`.